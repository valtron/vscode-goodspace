'use strict';

import * as vscode from 'vscode';
import { Settings } from './settings';

export class Goodspace {
    private settings: Settings;
    
    constructor() {
        this.settings = Settings.getInstance();
    }
    
    public highlight(editor: vscode.TextEditor, editorEdit: vscode.TextEditorEdit | undefined = undefined): void {
        const document = editor.document;
        if (this.ignoreDocument(document.languageId, document.uri.scheme)) return;
        editor.setDecorations(this.settings.textEditorDecorationType, this.getDecorations(document, editor.selection));
    }
    
    private getDecorations(document: vscode.TextDocument, selection: vscode.Selection): vscode.DecorationOptions[] {
        let decorations: vscode.DecorationOptions[] = [];
        const regex = new RegExp('^(\\t*)(\\s*)(.*?)(\\s*?)$');
        const lineCount = document.lineCount;
        const blankIndents: number[] = [];
        let indentLevel = -1;
        for (let i = 0; i < lineCount; ++i) {
            const line = document.lineAt(i);
            const [indent, prespace, text, postspace] = decompose(regex, line.text);
            
            if (prespace) {
                decorations.push(dec(i, indent, prespace, "Leading spaces"));
            }
            
            if (postspace) {
                decorations.push(dec(i, indent + prespace + text, postspace, "Trailing spaces"));
            }
            
            if (text == 0) {
                blankIndents.push(indent);
                continue;
            }
            
            const mx = indentLevel + 1;
            if (indent > mx) {
                decorations.push(dec(i, mx, indent - mx, "Too much indentation"));
            } else {
                indentLevel = indent;
            }
            
            if (blankIndents.length > 0) {
                highlightBlankIndents(i, Math.min(mx, indent), blankIndents, decorations);
            }
        }
        
        if (blankIndents.length > 0) {
            highlightBlankIndents(lineCount, 0, blankIndents, decorations);
        }
        
        if (!this.settings.highlightCurrentLine) {
            const r = document.lineAt(document.validatePosition(selection.end)).range;
            decorations = decorations.filter(d => (d.range.intersection(r) == undefined));
        }
        
        return decorations;
    }
    
    private ignoreDocument(language: string, scheme: string): boolean {
        return (
            (language != null) && this.settings.languagesToIgnore[language]
            || (scheme != null) && this.settings.schemesToIgnore[scheme]
        );
    }
}

function highlightBlankIndents(
    i: number, indent: number, blankIndents: number[], decorations: vscode.DecorationOptions[],
): void {
    const blankStart = i - blankIndents.length;
    for (let j = 0; j < blankIndents.length; ++j) {
        const bi = blankIndents[j];
        if (bi == indent) continue;
        const lineno = blankStart + j;
        let decoration: vscode.DecorationOptions;
        if (bi > indent) {
            decoration = dec(lineno, indent, bi - indent, "Too much indentation");
        } else {
            decoration = dec(lineno, bi, indent - bi, "Not enough indentation");
        }
        decorations.push(decoration);
    }
    blankIndents.splice(0, blankIndents.length);
}

function decompose(r: RegExp, l: string): [number, number, number, number] {
    const [_, indent, prespace, text, postspace] = r.exec(l)!;
    return [indent.length, prespace.length, text.length, postspace.length];
}

function dec(lineno: number, a: number, l: number, m: string): vscode.DecorationOptions {
    return {
        range: new vscode.Range(
            new vscode.Position(lineno, a),
            new vscode.Position(lineno, a + l)
        ),
        hoverMessage: m
    };
}
