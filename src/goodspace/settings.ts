'use strict';

import * as vscode from 'vscode';

export class Settings {
    private static instance: Settings = new Settings();
    
    highlightCurrentLine!: boolean;
    languagesToIgnore!: { [id: string]: boolean; };
    schemesToIgnore!: { [id: string]: boolean; };
    textEditorDecorationType!: vscode.TextEditorDecorationType;
    
    public static getInstance(): Settings {
        return Settings.instance;
    }
    
    private constructor() {
        if (Settings.instance) return;
        Settings.instance = this;
        this.refreshSettings();
    }
    
    public refreshSettings(): void {
        const config = vscode.workspace.getConfiguration('goodspace');
        this.highlightCurrentLine = config.get<boolean>('highlightCurrentLine');
        this.languagesToIgnore = this.getMapFromStringArray(config.get<string[]>('syntaxIgnore'));
        this.schemesToIgnore = this.getMapFromStringArray(config.get<string[]>('schemeIgnore'));
        this.textEditorDecorationType = this.getTextEditorDecorationType(config.get<string>('backgroundColor'), config.get<string>('borderColor'));
    }
    
    public resetToDefaults(): void {
        const config = vscode.workspace.getConfiguration('goodspace');
        config.update('highlightCurrentLine', undefined, true);
        config.update('syntaxIgnore', undefined, true);
        config.update('schemeIgnore', undefined, true);
        config.update('backgroundColor', undefined, true);
        config.update('borderColor', undefined, true);
        this.refreshSettings();
    }
    
    private getMapFromStringArray(array: string[]): { [id: string]: boolean; } {
        const map: { [id: string]: boolean; } = {};
        array.forEach((element: string) => {
            map[element] = true;
        });
        return map;
    }
    
    private getTextEditorDecorationType(backgroundColor: string, borderColor: string): vscode.TextEditorDecorationType {
        return vscode.window.createTextEditorDecorationType({
            borderRadius: "3px",
            borderWidth: "1px",
            borderStyle: "solid",
            backgroundColor: backgroundColor,
            borderColor: borderColor
        });
    }
}
