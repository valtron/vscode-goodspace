'use strict';

import * as vscode from 'vscode';
import { Settings } from './goodspace/settings';
import { Goodspace } from './goodspace/goodspace';

export function activate(context: vscode.ExtensionContext) {
    new Loader(context.subscriptions);
}

class Loader {
    private settings: Settings;
    private impl: Goodspace;
    private listenerDisposables: vscode.Disposable[] | null;
    
    constructor(subscriptions: vscode.Disposable[]) {
        this.settings = Settings.getInstance();
        this.impl = new Goodspace();
        this.listenerDisposables = null;
        this.initialize(subscriptions);
    }
    
    private initialize(subscriptions: vscode.Disposable[]): void {
        subscriptions.push(this);
        vscode.workspace.onDidChangeConfiguration(this.reinitialize, this, subscriptions);
        this.registerEventListeners();
        this.highlightActiveEditors()
    }
    
    private reinitialize(): void {
        this.dispose();
        this.settings.refreshSettings();
        this.registerEventListeners();
        this.highlightActiveEditors()
    }
    
    private registerEventListeners(): void {
        const disposables: vscode.Disposable[] = [];
        this.listenerDisposables = disposables;
        
        disposables.push(
            vscode.window.onDidChangeActiveTextEditor((editor: vscode.TextEditor | undefined) => {
                if (editor == null) return;
                this.impl.highlight(editor);
            }),
            vscode.workspace.onDidChangeTextDocument((event: vscode.TextDocumentChangeEvent) => {
                if (vscode.window.activeTextEditor == null) return;
                if (vscode.window.activeTextEditor.document != event.document) return;
                this.impl.highlight(vscode.window.activeTextEditor);
            }),
            vscode.workspace.onDidOpenTextDocument((document: vscode.TextDocument) => {
                if (vscode.window.activeTextEditor == null) return;
                if (vscode.window.activeTextEditor.document != document) return;
                this.impl.highlight(vscode.window.activeTextEditor);
            })
        );

        if (this.settings.highlightCurrentLine) {
            return;
        }
        
        disposables.push(
            vscode.window.onDidChangeTextEditorSelection((event: vscode.TextEditorSelectionChangeEvent) => {
                this.impl.highlight(event.textEditor);
            })
        );
    }

    private highlightActiveEditors(): void {
        vscode.window.visibleTextEditors.forEach(editor => this.impl.highlight(editor));
    }

    public dispose(): void {
        if (this.listenerDisposables == null) return;
        this.listenerDisposables.forEach(disposable => disposable.dispose());
    }
}
